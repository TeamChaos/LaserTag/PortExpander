#define F_CPU  8000000UL

#include <version.h>
#include "CommonLib/avr_libs/timers.h"
#include "CommonLib/avr_libs/TWI_slave.h"
#include "CommonLib/port_expander_control/protocol.h"


#define MOTOR_PORT PORTA
#define MOTOR_DDR DDRA

#define EN_MOTOR1 (1 << PA7)
#define EN_MOTOR2 (1 << PA6)
#define EN_MOTOR3 (1 << PA5)
#define EN_MOTOR4 (1 << PA4)
#define EN_MOTOR5 (1 << PA3)
#define EN_MOTOR6 (1 << PA1)

#define ISD_PORT PORTB
#define ISD_DDR DDRB
#define ISD_PIN PINB
#define ISD_MISO (1 << PB3)
#define ISD_SCLK (1 << PB4)
#define ISD_SSB (1 << PB5)
#define ISD_MOSI (1 << PB6)


#define DURATION_SHORT 50
#define DURATION_LONG 100

uint8_t onI2CRequest(uint8_t addr);
void onI2CReceive(uint8_t addr, uint8_t data);

const char gitversion[8] = GIT_COMMIT_HASH;

//Disable in x*10ms counter
uint8_t counter_m1 = 0;
uint8_t counter_m2 = 0;
uint8_t counter_m3 = 0;
uint8_t counter_m4 = 0;
uint8_t counter_m5 = 0;
uint8_t counter_m6 = 0;

uint8_t isd_check_result1 = 0xFF;
uint8_t isd_check_result2 = 0xFF;
uint8_t run_isd_check = 0;

uint8_t error_counter = 0;

/**
 * If not 0 the given sound is played during the next cycle
 */
uint8_t play_sound = 0x00;



/*Debug SPI
uint8_t spiWrite(uint8_t data1, uint8_t data2, uint8_t data3, uint8_t data4)
{

    ISD_PORT &= ~(ISD_SSB);
    delay(1);

    uint8_t bit;
    uint8_t in=0;
    for(bit = 0x80; bit; bit >>= 1) {
        ISD_PORT &= ~ISD_SCLK;
        if(data1 & bit) ISD_PORT |= ISD_MOSI;
        else ISD_PORT &= ~ISD_MOSI;
        ISD_PORT |= ISD_SCLK;
        if(ISD_PIN & ISD_MISO){
            in |= bit;
        }
    }
    for(bit = 0x80; bit; bit >>= 1) {
        ISD_PORT &= ~ISD_SCLK;
        if(data2 & bit) ISD_PORT |= ISD_MOSI;
        else ISD_PORT &= ~ISD_MOSI;
        ISD_PORT |= ISD_SCLK;
    }
    for(bit = 0x80; bit; bit >>= 1) {
        ISD_PORT &= ~ISD_SCLK;
        if(data3 & bit) ISD_PORT |= ISD_MOSI;
        else ISD_PORT &= ~ISD_MOSI;
        ISD_PORT |= ISD_SCLK;
    }
    for(bit = 0x80; bit; bit >>= 1) {
        ISD_PORT &= ~ISD_SCLK;
        if(data4 & bit) ISD_PORT |= ISD_MOSI;
        else ISD_PORT &= ~ISD_MOSI;
        ISD_PORT |= ISD_SCLK;
    }
    delay(1);
    ISD_PORT |= ISD_SSB;
    return in;
}
*/

// Fancy new SPI
void isdWriteRead(uint8_t *data) {
    for (uint8_t bit = 0x80; bit; bit >>= 1) {
        ISD_PORT &= ~ISD_SCLK;
        if (*data & bit) ISD_PORT |= ISD_MOSI;
        else
            ISD_PORT &= ~ISD_MOSI;
        ISD_PORT |= ISD_SCLK;
        if (ISD_PIN & ISD_MISO) {
            *data |= bit;
        } else {
            *data &= ~(bit);
        }
    }
}

void isdWrite(uint8_t count, uint8_t *data) {
    ISD_PORT &= ~(ISD_SSB);
    delay(1);
    for (uint8_t i = 0; i < count; i++) {
        isdWriteRead(data + i);
    }
    delay(1);
    ISD_PORT |= ISD_SSB;
}

uint8_t isdWrite1(uint8_t data1) {
    uint8_t data[1];
    data[0] = data1;
    isdWrite(1, data);
    return data[0];
}

uint8_t isdWrite2(uint8_t data1, uint8_t data2) {
    uint8_t data[2];
    data[0] = data1;
    data[1] = data2;
    isdWrite(2, data);
    return data[0];
}

uint8_t isdWrite3(uint8_t data1, uint8_t data2, uint8_t data3) {
    uint8_t data[3];
    data[0] = data1;
    data[1] = data2;
    data[2] = data3;
    isdWrite(3, data);
    return data[0];
}

uint8_t isdWrite4(uint8_t data1, uint8_t data2, uint8_t data3, uint8_t data4) {
    uint8_t data[4];
    data[0] = data1;
    data[1] = data2;
    data[2] = data3;
    data[3] = data4;
    isdWrite(4, data);
    return data[0];
}

void checkISD() {
    uint8_t data[4];
    data[0] = 0x40;
    data[1] = 0x00;
    data[2] = 0x00;
    data[3] = 0x00;
    isdWrite(4, data);
    isd_check_result1 = data[0];
    isd_check_result2 = data[1];
}


void playSound(uint8_t id) {
    checkISD();
    if (~isd_check_result1 & 0b01000000) {
        //Not ready
        error_counter++;
        return;
    } else if (isd_check_result1 & 0b10) {
        //Channel still busy
        isdWrite1(0x2a);
        error_counter++;
    } else if (isd_check_result2 & 0b10000) {
        //There has been an error before
        isdWrite4(0x46, 0x00, 0x00, 0x00);//Clear interrupts
        error_counter++;
        return;
    }
    isdWrite3(0xa6, 0x00, id);

}


void onI2CReceive(uint8_t addr, uint8_t data) {
    if (addr == I2C_MOTOR_LONG || addr == I2C_MOTOR_SHORT) {
        uint8_t time = addr == I2C_MOTOR_SHORT ? DURATION_SHORT : DURATION_LONG;
        if (data & 0b1) {
            counter_m1 = time;
            MOTOR_PORT |= EN_MOTOR1;
        }
        if (data & 0b10) {
            counter_m2 = time;
            MOTOR_PORT |= EN_MOTOR2;
        }
        if (data & 0b100) {
            counter_m3 = time;
            MOTOR_PORT |= EN_MOTOR3;
        }
        if (data & 0b1000) {
            counter_m4 = time;
            MOTOR_PORT |= EN_MOTOR4;
        }
        if (data & 0b10000) {
            counter_m5 = time;
            MOTOR_PORT |= EN_MOTOR5;
        }
        if (data & 0b100000) {
            counter_m6 = time;
            MOTOR_PORT |= EN_MOTOR6;
        }
    } else if (addr == I2C_ISD_RUN_CHECK) {
        run_isd_check = 1;
    } else if (addr == I2C_PLAY_SOUND) {
        play_sound = data;
    }

}

uint8_t onI2CRequest(uint8_t addr) {
    switch (addr) {
        case I2C_GET_OK:
            return I2C_ALL_OK;
        case I2C_ISD_GET_CHECK1:
            return isd_check_result1;
        case I2C_ISD_GET_CHECK2:
            return isd_check_result2;
        case I2C_GET_ERROR_COUNT:
            return error_counter;
        case I2C_READ_VERSION1:
        case I2C_READ_VERSION2:
        case I2C_READ_VERSION3:
        case I2C_READ_VERSION4:
        case I2C_READ_VERSION5:
        case I2C_READ_VERSION6:
        case I2C_READ_VERSION7:
        case I2C_READ_VERSION8:
            return (uint8_t) gitversion[addr - I2C_READ_VERSION1];
        default:
            return 0;
    }
}




//################### Main Code ###################### //

void loop() {
    if (counter_m1 > 0) {
        if (--counter_m1 == 0) {
            MOTOR_PORT &= ~(EN_MOTOR1);
        }
    }
    if (counter_m2 > 0) {
        if (--counter_m2 == 0) {
            MOTOR_PORT &= ~(EN_MOTOR2);
        }
    }
    if (counter_m3 > 0) {
        if (--counter_m3 == 0) {
            MOTOR_PORT &= ~(EN_MOTOR3);
        }
    }
    if (counter_m4 > 0) {
        if (--counter_m4 == 0) {
            MOTOR_PORT &= ~(EN_MOTOR4);
        }
    }
    if (counter_m5 > 0) {
        if (--counter_m5 == 0) {
            MOTOR_PORT &= ~(EN_MOTOR5);
        }
    }
    if (counter_m6 > 0) {
        if (--counter_m6 == 0) {
            MOTOR_PORT &= ~(EN_MOTOR6);
        }
    }
    if (run_isd_check) {
        checkISD();

        run_isd_check = 0;
    }
    if (play_sound != 0x00) {
        playSound(play_sound);
        play_sound = 0x00;
    }
    delay(10);
}

void setup() {

    i2c_init(PORTEXPANDER_I2C_ADDRESS);
    i2c_onRequestPtr = onI2CRequest;
    i2c_onReceivePtr = onI2CReceive;

    MOTOR_DDR |= EN_MOTOR1 | EN_MOTOR2 | EN_MOTOR3 | EN_MOTOR4 | EN_MOTOR5 | EN_MOTOR6;

    ISD_DDR |= ISD_SCLK | ISD_MOSI | ISD_SSB;
    ISD_DDR &= ~(ISD_MISO);
    ISD_PORT |= ISD_SCLK | ISD_SSB;



}



int main(void) {

    initTimers();

    DDRA |= EN_MOTOR6;
    PORTA |= EN_MOTOR6;
    delay(200);
    PORTA &= ~(EN_MOTOR6);
    delay(200);
    PORTA |= EN_MOTOR6;
    delay(200);
    PORTA &= ~(EN_MOTOR6);
    setup();

    while (1) {
        loop();
    }
    return 0;
}


