# Port Expander Code 

This code controlls the AVR microcontroller used as "intelligent" port expander on the main PCB.

The exact microcontroller used here is the MicroChip Attiny 861A.
However, a 261A or 461 should work fine too

[Datasheet](http://ww1.microchip.com/downloads/en/DeviceDoc/doc8197.pdf)

## AVR Configuration
The microcontroller has to run with full 8 MHz without clock divider. It is also recommend to enable brown-out detection.  
Recommended fuse settings:  
``` -U lfuse:w:0xe2:m -U hfuse:w:0xdd:m -U efuse:w:0xff:m  ```

If you use a different device or frequency you will have to adjust the CMakeList.txt:
```
SET(DEVICE "attiny861")
SET(FREQ "8000000")
add_definitions(-DATTINY861)
```

## Function
This handles two main tasks
1. Control (PWM) LEDs
2. Receive commands from player master (ESP) via I2C

### Pins
- 18 (SCL): I2C clock
- 20 (SDA): I2C data
- 8 (PA3): Status LED 1
- 4 (PA4): Status LED 2
- 7 (PA5): RGB - Red
- 9 (PA6): RGB - Green
- 11 (PA7): RGB - Blue

## Code
### Program structure+
We basically wait for I2C instructions to enable/disable LEDs.  
The PWM LEDs are controlled by a software PWM since we do not need high frequencies or perfect signal shapes. Thereby we can use any PIN as PWM output.  
A counter triggers a overflow interrupt with 10.000 Hz. The interrupt has another counter (up to 100). On reset all LEDs are enabled,
once the counter reaches the PWM value set for them (e.g. 40) it is disabled again.
This results in a switching frequency of 100Hz which should be high enough to no be noticed.

## Development Setup
To work on this project you need the several packages:  
avr-binutils, avr-gcc, and avr-libc  
Don't forget about git of course.  

A CMake compatible IDE is recommended.  
E.g. CLion or KDevelop (free)  
QtCreator is supposed to work as well

